<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GetData;

class GetsController extends Controller
{
    public function index(){

    	//$gets = GetData::all();
    	$gets = GetData::orderby('start_date_time','desc')->paginate(10);

    	return view('rawdata/index')->with(['gets'=>$gets]);
    }
    public function show($id){
    	return view('rawdata/show');
    }
    public function rawdatag($from, $to){
        $from="2016-04-07 12:00:00";
        $to="2016-04-07 12:01:00";
        $graph = GetData::whereBetween('start_date_time', [$from, $to])->get()->toJson();
                //where('start_date_time','>=','$from')
                //->where('start_date_time','<=','$to')
        if (!$graph){
           return response()->json([
                    //'STATUS'=> false,
                    //'MESSAGE'=>'Datos nuelos',
                    'DATA' => null
            ], 200);
        } 
        else {
            return response()->json(array($graph),200);
            //return response()->json($graph);
                  /* 'STATUS'=> true,
                    'MESSAGE'=>'success',
                    'DATA' => $graph
            ], 200*/
           // );
            echo json_encode($graph);
        }
     
        //$user_id = User::select('id')->where('username', $username)->first();
        //SELECT * FROM `bintable` WHERE `start_date_time` BETWEEN "2016-04-07 12:00:00" AND "2016-04-07 13:00:00"
        //$users = DB::table('users')->whereBetween('votes', [1, 100])->get();
        //return json_encode($graph);
        //return view('rawdata/rawdata')->with(['gets'=>$graph]);
        //return Response::->Json($graph);
    }
}
