<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Getdata extends Model{
	protected $table ='bintable';
	protected $fillable =['start_date_time','ch01','ch02', 'ch03', 'ch04', 'ch05', 'ch06', 'ch07', 'ch08', 'ch09', 'ch10', 'ch11', 'ch12', 'ch13', 'ch14', 'ch15', 'ch16', 'ch17', 'ch18', 'hv1', 'hv2', 'hv3', 'temp_1', 'temp_2', 'atmPressure'];
}