<?php

Route::get('/rawdata','GetsController@index'); //ruta de la vista
Route::get('rawdata/{id}','GetsController@show');
Route::resource('rawdata','GetsController');
Route::get('rawdata/from/{from}/to/{to}','GetsController@rawdatag');